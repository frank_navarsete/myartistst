package com.glitchcube.boundary;

import com.airhacks.rulz.jaxrsclient.JAXRSClientProvider;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.json.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ArtistResourceTest {


    @Rule
    public JAXRSClientProvider provider = JAXRSClientProvider.buildWithURI("http://localhost:8080/myartist/resources/artist");

    @Test
    public void shouldCreateAnArtist() {
        Response postResponse = createAndSaveArtist("Dragon Force");

        assertThat(postResponse.getStatus(), is(Response.Status.CREATED.getStatusCode()));
    }

    @Test
    public void shouldBeAbleToFindSavedArtistById() {
        String artistName = "Freedom Call";
        Response postResponse = createAndSaveArtist(artistName);

        String savedArtistLocation = postResponse.getHeaderString("Location");

        JsonObject artistLocatedById = provider.target(savedArtistLocation)
                .request(MediaType.APPLICATION_JSON)
                .get(JsonObject.class);

        assertThat(artistLocatedById.getString("name"), is(artistName));

    }

    @Test
    public void shouldBeAbleToUpdateArtistById() {
        Response postResponse = createAndSaveArtist("Freedom Cal");

        String savedArtistLocation = postResponse.getHeaderString("Location");

        JsonObject artistWithUpdatedName = Json.createObjectBuilder()
                .add("name", "Freedom Call")
                .build();

        provider.target(savedArtistLocation)
                .request()
                .put(Entity.json(artistWithUpdatedName));

        JsonObject updatedArtist = provider.target(savedArtistLocation)
                .request(MediaType.APPLICATION_JSON)
                .get(JsonObject.class);

        assertThat(updatedArtist.getString("name"), is("Freedom Call"));

    }

    @Test
    public void shouldBeAbleToFindAlleArtist() {
        createAndSaveArtist("Sonata Arctica");
        createAndSaveArtist("Heavenly");

        JsonArray allArtists = fetchAllArtists();

        assertThat(allArtists.isEmpty(), is(false));
        System.out.println(allArtists);
    }

    @Test
    public void shouldBeAbleToAnArtistById() {
        Response postResponse = createAndSaveArtist("Freedom Call");

        String savedArtistLocation = postResponse.getHeaderString("Location");

        JsonArray allArtistsBeforeDelete = fetchAllArtists();


        provider.target(savedArtistLocation)
                .request()
                .delete();

        JsonArray allArtistsAfterDelete = fetchAllArtists();

        assertThat(allArtistsBeforeDelete.size() - allArtistsAfterDelete.size(), is(1));
    }

    @Test
    public void shouldBeAbleToDeleteAllArtists() {
        createAndSaveArtist("Dream Evil");
        createAndSaveArtist("Sabaton");

        assertThat(fetchAllArtists().isEmpty(), is(false));

        provider.target()
                .request()
                .delete();

        assertThat(fetchAllArtists().isEmpty(), is(true));
    }


    private Response createAndSaveArtist(String name) {
        JsonObject artistToCreate = Json.createObjectBuilder()
                .add("name", name)
                .build();

        return provider.target()
                .request()
                .post(Entity.json(artistToCreate));
    }

    private JsonArray fetchAllArtists() {
        return provider.target()
                .request(MediaType.APPLICATION_JSON)
                .get(JsonArray.class);
    }
}
